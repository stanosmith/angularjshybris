1. [hybris_controllers/](https://bitbucket.org/stanosmith/angularjshybris/src/master/hybris_controllers) contains files to copy to hybris extensions
1. Look through the following files:
    - [src/demo-app/services/hybris.cms.service.ts](https://bitbucket.org/stanosmith/angularjshybris/src/master/src/demo-app/services/hybris.cms.service.ts)
    - [src/demo-app/services/product.service.ts](https://bitbucket.org/stanosmith/angularjshybris/src/master/src/demo-app/services/product.service.ts)

There is an intentionally hardcoded token. You need to update it (see comments there) or to add a request to the code.
```
curl -k "https://localhost:9002/authorizationserver/oauth/token" -d "client_secret=secret&grant_type=client_credentials&client_id=trusted_client"
``` 
