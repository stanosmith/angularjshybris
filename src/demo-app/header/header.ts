
import {Component} from '@angular/core';
import {HybrisCMSService} from '../services/hybris.cms.service';
import {Page} from '../data/page';

@Component({
  moduleId: module.id,
  selector: 'my-header',
  templateUrl: 'header.html'
})
export class Header {
  errorMessage : string;
  page : Page = { contents: "waiting..." }
  section1 : string;

  constructor(private hybrisCMSService: HybrisCMSService) { }

  ngOnInit(): void {
    //this.getHeader();
    this.hybrisCMSService.loadSlotHTMLIntoDiv('SearchBox', 'SearchBox').subscribe(
      str => this.hybrisCMSService.saveHTMLIntoDiv(str, 'SearchBox', 'SearchBox')
    );
    this.hybrisCMSService.loadSlotHTMLIntoDiv('Section1', 'Section1').subscribe(
      str => this.hybrisCMSService.saveHTMLIntoDiv(str, 'Section1', 'Section1')
    );

  }
  getHeader() : void {
    /*this.hybrisCMSService.getPage("header").subscribe(
                   page         => this.page = page,
                   error        => this.errorMessage = <any>error
                );
*/

  }
}
