import { Component, Input } from '@angular/core';
import { Product } from '../data/product';
@Component({
  selector: 'my-product-detail',
  template: `
    <div *ngIf="product">
      <b>{{product.name}} details!</b>
      <div><label>id: </label>{{product.code}}</div>
      <div>
        <label>product: </label>
	{{product.name}}
	<hr>
	<ul>
	<li><b>Summary:</b> {{product.description}}
	<li><b>Price:</b> {{product.price}}
<!--        <input [(ngModel)]="product.name" placeholder="name"/>-->
	</ul>
      </div>
    </div>
  `
})
export class ProductDetailComponent {
  @Input()
  product: Product;
}
