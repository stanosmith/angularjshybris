import {NgModule, ApplicationRef} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import {DemoApp, Home} from './demo-app/demo-app';
import {RouterModule} from '@angular/router';
import {MaterialModule} from '@angular2-material/all/all';
import {DEMO_APP_ROUTES} from './demo-app/routes';
import {ListDemo} from './list/list-demo';
import {Header} from './header/header';
import {HybrisCMSService} from './services/hybris.cms.service';
import {ProductService} from './services/product.service';
import {ProductDetailComponent} from './list/product-detail.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(DEMO_APP_ROUTES),
    MaterialModule.forRoot(),
  ],
  declarations: [
    Home,
    ListDemo,
    Header,
    ProductDetailComponent
  ],
  providers: [
    HybrisCMSService,
    ProductService
  ],
  entryComponents: [
    DemoApp
  ],
})
export class DemoAppModule {
  constructor(private _appRef: ApplicationRef) { }

  ngDoBootstrap() {
    this._appRef.bootstrap(DemoApp);
  }
}
