import { Injectable } from '@angular/core';
import { Page } from '../data/page'
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import {BrowserDomAdapter} from '@angular/platform-browser/src/browser/browser_adapter';
import 'rxjs/Rx';

@Injectable()
export class HybrisCMSService {
 cmsUrl:string = "https://electronics.local:9002/trainingstorefront/cmsservice?pageId=/AngularTemplatePage&slotId=";
 host:string   = "https://electronics.local:9002";
 http : Http;

 constructor(http: Http){
       this.http = http;
 }


 loadSlotHTMLIntoDiv(componentId : string, divid : string) : Observable<string> {
    return this.http.get(this.cmsUrl + componentId)
                   .map(this.extractData);
 }
 saveHTMLIntoDiv(data : string, componentId : string, divid : string) : void
 {
   let domAdapter = new BrowserDomAdapter();
   data = data.split("src=\"/medias/").join("src=\""+this.host+"/medias/");
   domAdapter.setInnerHTML(document.getElementById(divid), data);
 }

 /*getPage(pageId : string) : Observable<Page> {
   // todo: add pageId to the request
   return this.http.get(this.cmsUrl)
                   .map(this.extractData);
  }
  */
 private extractData (res : Response) : string {
   return res.text();
 }
 /*private extractData(res: Response) : Page {
   let body = res.text();
   let page = new Page();
   page.contents = body;
   //console.log(document.getElementById('my-header').innerHtml);
   //document.getElementById('my-header').innerHtml = body;
   let domAdapter = new BrowserDomAdapter();
   domAdapter.setInnerHTML(document.getElementById('my-header'), body);
   return page;
 }
 */

 private handleError (error: any) {
   let errMsg = (error.message) ? error.message :
   error.status ? `${error.status} - ${error.statusText}` : 'Server error';
   console.error(errMsg); // log to console instead
   return Promise.reject(errMsg);
 }

}
